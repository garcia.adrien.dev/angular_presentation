import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddViewComponent } from './views/add-view/add-view.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { DetailViewComponent } from './views/detail-view/detail-view.component';
import { EditViewComponent } from './views/edit-view/edit-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { SerieViewComponent } from './views/serie-view/serie-view.component';
import { SerieTableComponent } from './components/serie-table/serie-table.component';
import { SerieTableRowComponent } from './components/serie-table-row/serie-table-row.component';
import { AuthService } from './services/auth/auth.service';
import { SerieService } from './services/serie/serie.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SerieFormComponent } from './components/serie-form/serie-form.component';
import { CommentaryRowComponent } from './components/commentary-row/commentary-row.component';
import { CommentaryAddViewComponent } from './views/commentary-add-view/commentary-add-view.component';
import { CommentFormComponent } from './components/comment-form/comment-form.component';

@NgModule({
  declarations: [
    AppComponent,
    AddViewComponent,
    AuthViewComponent,
    DetailViewComponent,
    EditViewComponent,
    NotFoundViewComponent,
    SerieViewComponent,
    SerieTableComponent,
    SerieTableRowComponent,
    SerieFormComponent,
    CommentaryRowComponent,
    CommentaryAddViewComponent,
    CommentFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthService, SerieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
