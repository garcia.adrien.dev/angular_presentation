import { Commentary } from "./commentary-model";

export class Serie {

  private _id: number;
  private _name: string;
  private _date: Date;
  private _season: number;
  private _description: string;
  private _critics: string;
  private _photo: string;
  private _commentary: Array<Commentary>;

  constructor(id: number, name: string, date: Date, season: number, description: string, critics: string,
    photo: string, commentary: Array<Commentary>){
      this._id = id;
      this._name = name;
      this._date = date;
      this._season = season;
      this._description = description;
      this._critics = critics;
      this._photo = photo;
      this._commentary = commentary;
    }

     get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

   get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

   get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }

   get season(): number {
    return this._season;
  }

  set season(value: number) {
    this._season = value;
  }

   get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }
   get critics(): string {
    return this._critics;
  }

  set critics(value: string) {
    this._critics = value;
  }

   get photo(): string {
    return this._photo;
  }

  set photo(value: string) {
    this._photo = value;
  }

   get commentary(): Array<Commentary>{
    return this._commentary;
  }

  set commentary(value:Array<Commentary>) {
    this._commentary = value;
  }
  }
