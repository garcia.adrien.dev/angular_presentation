import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Commentary } from '../../modele/commentary-model';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  @Input() comment: Commentary;
   @Input() submitLabel: string;
  @Output() submitForm: EventEmitter<Commentary>

  commentForm: FormGroup

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.submitForm = new EventEmitter<Commentary>();
   }

  ngOnInit(): void {
    this._initForm()
  }

   onSubmitForm(): void{
    this.comment.author = this.authService.username;
    this.comment.date = new Date();
    this.submitForm.emit(this.comment)
  }

  private _initForm(): void{
    this.commentForm = this.fb.group({
      content: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(200)]],
  })
   }

}
