import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentaryRowComponent } from './commentary-row.component';

describe('CommentaryRowComponent', () => {
  let component: CommentaryRowComponent;
  let fixture: ComponentFixture<CommentaryRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentaryRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentaryRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
