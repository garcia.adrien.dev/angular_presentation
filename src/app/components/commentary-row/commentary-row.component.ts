import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-commentary-row]',
  templateUrl: './commentary-row.component.html',
  styleUrls: ['./commentary-row.component.css']
})
export class CommentaryRowComponent implements OnInit {

  @Input() id: number;
  @Input() author: string;
  @Input() date: Date;
  @Input() content: string;

  constructor() { }

  ngOnInit(): void {
  }

}
