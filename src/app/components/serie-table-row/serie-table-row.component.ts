import { Component, Input, OnInit } from '@angular/core';
import { SerieService } from 'src/app/services/serie/serie.service';


@Component({
  selector: '[app-serie-table-row]',
  templateUrl: './serie-table-row.component.html',
  styleUrls: ['./serie-table-row.component.css']
})
export class SerieTableRowComponent implements OnInit {

  @Input() id: number;
  @Input() name: string;
  @Input() date: Date;

  constructor(private serieService: SerieService) { }

  ngOnInit(): void {
  }

  onClickDeleteButton(): void{
    this.serieService.delete(this.id);
  }
}
