import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Serie } from 'src/app/modele/serie-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-serie-form',
  templateUrl: './serie-form.component.html',
  styleUrls: ['./serie-form.component.css']
})
export class SerieFormComponent implements OnInit {

  @Input() serie: Serie;
  @Input() submitLabel: string;
  @Output() submitForm: EventEmitter<Serie>

  serieForm: FormGroup


  constructor(private fb: FormBuilder) {
    this.submitForm = new EventEmitter<Serie>();
   }

  ngOnInit(): void {
    this._initForm();
  }

  onSubmitForm(): void{
    this.submitForm.emit(this.serie)
  }

   private _initForm(): void{
    this.serieForm = this.fb.group({
      name:[null, [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      season: [null,[Validators.required, Validators.min(1)]],
      date: [null, [Validators.required]],
      description: [null, [Validators.required, Validators.minLength(20), Validators.maxLength(200)]],
      critics: [null, [Validators.required, Validators.minLength(20), Validators.maxLength(200)]],
      photo: [null, [Validators.required]],
  })
   }
}
