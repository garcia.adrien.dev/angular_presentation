import { Component, OnInit } from '@angular/core';
import { Serie } from 'src/app/modele/serie-model';
import { SerieService } from '../../services/serie/serie.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-view',
  templateUrl: './add-view.component.html',
  styleUrls: ['./add-view.component.css']
})
export class AddViewComponent implements OnInit {

  serie: Serie;

  constructor(private serieService: SerieService, private router: Router) {
    this.serie = new Serie(null, null, null, null,null,null,null, null)
  }

  ngOnInit(): void {
  }

   onSubmitNewSerieForm(serieToAdd: Serie): void{
this.serieService.add(serieToAdd).then(()=> this.router.navigateByUrl('/serie'))
  }

}
