import { Component, OnInit } from '@angular/core';
import { Commentary } from '../../modele/commentary-model';
import { SerieService } from '../../services/serie/serie.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Serie } from 'src/app/modele/serie-model';

@Component({
  selector: 'app-commentary-add-view',
  templateUrl: './commentary-add-view.component.html',
  styleUrls: ['./commentary-add-view.component.css']
})
export class CommentaryAddViewComponent implements OnInit {

  comment: Commentary;
  serie: Serie;

  constructor(private serieService: SerieService, private router: Router, private route: ActivatedRoute) {
    this.comment = new Commentary(null, null, null, null)
   }

  ngOnInit(): void {
    const id  = this.route.snapshot.params.id
  this.serieService.getSerieById(+id).then(serie => this.serie = serie)
  }


  onSubmitNewCommentForm(commentToAdd: Commentary): void{
    this.serieService.addComment(this.serie, commentToAdd).then(()=> {
      this.router.navigateByUrl('serie/detail/' + this.serie.id)
    })
  }
}


