import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentaryAddViewComponent } from './commentary-add-view.component';

describe('CommentaryAddViewComponent', () => {
  let component: CommentaryAddViewComponent;
  let fixture: ComponentFixture<CommentaryAddViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentaryAddViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentaryAddViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
