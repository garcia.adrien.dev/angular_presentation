import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';
import { Serie } from 'src/app/modele/serie-model';
import { SerieService } from '../../services/serie/serie.service';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.css']
})
export class DetailViewComponent implements OnInit {

  serie: Serie;

  constructor(private route: ActivatedRoute, private serieService: SerieService) { }

  ngOnInit(): void {
     const id = this.route.snapshot.params.id;
    console.log(id);
    this.serieService.getSerieById(+id).then(serie => this.serie = serie)
  }

}
