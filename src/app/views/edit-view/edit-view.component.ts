import { Component, OnInit } from '@angular/core';
import { SerieService } from '../../services/serie/serie.service';
import { ActivatedRoute } from '@angular/router';
import { Serie } from 'src/app/modele/serie-model';


@Component({
  selector: 'app-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.css']
})
export class EditViewComponent implements OnInit {

  serie: Serie;

  msg: string;

  constructor(private serieService: SerieService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id  = this.route.snapshot.params.id
  this.serieService.getSerieById(+id).then(serie => this.serie = serie)  }

  onSubmitEditSerieForm(serieEdited: Serie): void{
    this.serieService.edit(serieEdited).then(msg => this.msg = msg)
  }

}
