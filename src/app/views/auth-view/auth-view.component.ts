import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css']
})
export class AuthViewComponent implements OnInit {

  errMsg: string;
  constructor(private authService: AuthService, private router: Router) {
     this.errMsg = ''
   }

  ngOnInit(): void {
  }

   onSubmitLogin(form: NgForm): void{
    const {username, password} = form.value;

  this.authService.signIn(username, password).then(()=> {
    this.router.navigateByUrl('serie');
  })
  .catch(errMsg => this.errMsg = errMsg)
  }

}
