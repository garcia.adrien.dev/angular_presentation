import { Component, OnInit } from '@angular/core';
import { Serie } from 'src/app/modele/serie-model';
import { SerieService } from 'src/app/services/serie/serie.service';

@Component({
  selector: 'app-serie-view',
  templateUrl: './serie-view.component.html',
  styleUrls: ['./serie-view.component.css']
})
export class SerieViewComponent implements OnInit {

    series: Array<Serie>
  constructor(private serieService: SerieService) { }

  ngOnInit(): void {
     this.series = this.serieService.series;
  }
  }


