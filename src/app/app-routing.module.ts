import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AddViewComponent } from './views/add-view/add-view.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { CommentaryAddViewComponent } from './views/commentary-add-view/commentary-add-view.component';
import { DetailViewComponent } from './views/detail-view/detail-view.component';
import { EditViewComponent } from './views/edit-view/edit-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { SerieViewComponent } from './views/serie-view/serie-view.component';

const routes: Routes = [
    {path:'', component: AuthViewComponent},
  {path:'serie', canActivate: [AuthGuard], component:SerieViewComponent },
  {path:'serie/new', canActivate: [AuthGuard], component:AddViewComponent},
  {path:'serie/detail/:id', canActivate: [AuthGuard], component:DetailViewComponent},
  {path:'serie/comm/:id', canActivate: [AuthGuard], component:CommentaryAddViewComponent},
  {path:'serie/edit/:id', canActivate: [AuthGuard], component:EditViewComponent},
  {path: 'not-found', component: NotFoundViewComponent},
  {path:'**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
