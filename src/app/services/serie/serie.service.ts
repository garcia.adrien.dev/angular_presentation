import { Injectable } from '@angular/core';
import { Commentary } from 'src/app/modele/commentary-model';
import { Serie } from 'src/app/modele/serie-model';


@Injectable({
  providedIn: 'root'
})
export class SerieService {

series: Array<Serie>;
  constructor() {
    this.series = []

    for (let i = 1; i <= 10; i++){
      this.series.push(
        new Serie(
          i,
          'Serie' + i,
          new Date(),
          4,
          'Serie numero ' + i,
          'J\'adore vraiemnt la serie' + i,
          'https://cdn.pixabay.com/photo/2013/07/13/10/52/anonymous-157950_960_720.png',

          [new Commentary(
            1,
            new Date,
            'Auteur 1',
            'On adore la serie'

          ),
        new Commentary(
            2,
            new Date,
            'Auteu 2' ,
            'On adore la serie'

          ),
        new Commentary(
            3,
            new Date,
            'Auteur 3',
            'On adore la serie'

          )]
        )
      )
     }
  }

  getSerieById(serieId: number): Promise<any>{
    const cb = (res, rej) => {
      for(const serie of this.series){
        if (serie.id === serieId) {
          res(serie)

        }
      }
    }
    return new Promise<any>(cb)
  }

  edit(serieEdited: Serie): Promise<string>{
    const cb = (res, rej) => {
      for (const [index, serie] of this.series.entries()){
        if (serie.id === serieEdited.id){
          this.series[index] = serieEdited;
          res('Livre modifier avec succes')
        }
      }
    }
    return new Promise<string>(cb)  }

  add(newSerie: Serie): Promise<void>{
    const cb = (res, rej) => {
      newSerie.id = this.series[this.series.length - 1].id + 1;


    if (this.series.length === 0){
      newSerie.id = 1;
    }

    this.series.push(newSerie);

    res();
}
    return new Promise<void>(cb)
  }

   delete(serieId: number): void{
      for (const [index, serie] of this.series.entries()){
        if (serie.id === serieId){
          this.series.splice(index, 1)
          break;
        }
      }
    }

  addComment(commentedSerie: Serie, commentToAdd: Commentary): Promise<void>{
    const cb = (res, rej) => {
     commentToAdd.id = commentedSerie.commentary[commentedSerie.commentary.length - 1].id + 1;

    if (commentedSerie.commentary.length === 0){
      commentToAdd.id = 1;
    }

    commentedSerie.commentary.push(commentToAdd);
    console.log(commentToAdd);

    res();
}
    return new Promise<void>(cb)
  }
 }
